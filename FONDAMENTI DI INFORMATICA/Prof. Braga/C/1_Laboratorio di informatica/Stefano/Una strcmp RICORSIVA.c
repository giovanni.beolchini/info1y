#include <stdio.h>
#include <string.h>
#include <math.h>

int main(){
	
	char str1[10]="capra";
	char str2[10]="caprarica";
	
	printf("%d\n", strcmp(str1, str2));
	printf("%d", strecursivecmp(str1, str2));
	
	return 0;
}

int strecursivecmp(char *s1, char *s2){	//� una strcmp ricorsiva!!
	if(*s1=='\0' && *s2=='\0'){
		return 0;
	}
	if(*s1==*s2){
		return strecursivecmp(s1+1, s2+1);
	}
	else if(*s1<*s2){
		return -1;
	}
	else if(*s1>*s2){
		return 1;
	}
}
