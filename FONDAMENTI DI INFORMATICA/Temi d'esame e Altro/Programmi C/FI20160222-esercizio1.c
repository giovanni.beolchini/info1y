/*
Si consideri una lista dinamica di interi definita come:

struct nodo
{
    int val;
    struct nodo *next;
};

typedef struct nodo *lista;

A.	Implementare la seguente funzione in modo iterativo (cioè senza ricorsione):

void separa(lista a, lista *pari, lista* dispari);

che riceve in ingresso la testa di una lista di interi, a, e i puntatori a due liste vuote di interi, pari e dispari; la funzione, quindi popola la lista
puntata dal parametro pari tutti gli elementi pari di a e popola la lista puntata a dal parametro dispari con tutti gli elementi dispari di a.

B.	Implementare una versione ricorsiva della funzione separa (senza cambiare il prototipo).

Nello svolgimento dei punti precedenti, è possibile utilizzare (senza bisogno di implementarla) la funzione

void inserisci (lista *l, int val)

che inserisce l’elemento val in testa alla lista l.

Note. Si può supporre che le due liste vuote puntate dai parametri pari e dispari passati alla funzione contengano il valore NULL. Se necessario, è possibile
implementare ulteriori funzioni di supporto da utilizzare all’interno della funzione separa. Non è importante l’ordine degli elementi inseriti nelle liste pari
e dispari.
*/

void separa_ric(lista a, lista *pari, lista* dispari) {
    if(a == NULL)
    else {
        if(a->val%2==0)
            inserisci(pari, a->val);
        else
            inserisci(dispari, a->val);

        separa_ric(a->next, pari, dispari);
    }
}

void separa_iter(lista a, lista *pari, lista* dispari) {
    while(a != NULL) {
        if(a->val%2==0)
            inserisci(pari, a->val);
        else
            inserisci(dispari, a->val);

        a = a->next;
    }
}
