/*
A)	Dichiarare un tipo di dato audiolibro che consenta di rappresentare le seguenti informazioni: titolo del libro, autore del libro, narratore del libro,
durata (ore, minuti, secondi) e la lista dei capitoli che compongono l’audiolibro (al massimo 100). Per ciascun capitolo dell’audiolibro, è necessario memorizzare
inizio (ore, minuti, secondi), fine (ore, minuti, secondi) e durata del capitolo (ore, minuti, secondi).

B)	Scrivere una funzione C, lunghi, che riceve in ingresso una variabile di tipo audiolibro e restituisce il numero di capitoli in esso contenuto che hanno
durata superiore a 30 minuti.

*/

#include <stdio.h>

typedef stuct {
    int ore;
    int minuti;
    int secondi;
} tempo;

typedef struct {
    tempo inizio;
    tempo fine;
    tempo durata;
} capitolo


typedef struct {
    char titolo[100];
    char autore[100];
    char narratore[100];
    tempo durata;
    capitolo capitoli[100];
    int numeroCapitoli;
} audiolibro;

int lunghi(audiolibro *audiolibro) {
    int i;
    int lunghi = 0;

    for(i = 0; i<audiolibro.numeroCapitoli; i++) {
        if(audiolibro.capitoli[i].durata.ore*60 + audiolibro.capitoli[i].durata.minuti > 30)
            lunghi++;
    }

    return lunghi;
}
