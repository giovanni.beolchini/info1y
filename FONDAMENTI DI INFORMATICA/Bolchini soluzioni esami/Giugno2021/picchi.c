#include <stdio.h>                       /*le liste non sono mai state viste a lezione con il main, quindi non posso provarle. Riporto una soluzione possibile vista ad esercitazione*/

typedef struct lista_s{
	int dato;                          /*solito modo per definire una lista per le gestione di numeri interi*/
	struct lista_s*next;
}lista_t


lista_t *listapicchi(lista_t*h){
	lista_t *picchi=NULL;                  /*dichiaro una nuova testa di lista, perch� ne va creata una nuova. Si setta sempre a NULL inizialmente*/
	lista_t *ptr, ptr2; 
	
	ptr=h;                
	
	while(ptr && ptr->next){
		ptr2=ptr->next;
		while(ptr2 && ptr->dato > ptr2->dato){
		ptr2=ptr2->next;
	}
	
	if(ptr2==NULL){                               /*se siamo usciti dal ciclo perch� ptr2 arriva a NULL vuol dire che tutti gli elementi trovati sono minori di ptr->dato, quindi aggiungo nella lista ptr->dato che sar� l'elemento picco*/
		picchi=append(picchi, ptr->dato);
	}
	
	ptr=ptr->next;
}

if(ptr!=NULL){                                      
	picchi=append(picchi, ptr->dato);         /*aggiungo ultimo elemento, come richiesto*/
}

return picchi;
}
	
	

