/*
Input: una successione di numeri interi >0 terminante con uno 0 (quando mette lo 0 la sequenza finisce)

Output:
    - "Crescente" se la successione è strettamente crescente
    - "Decrescente" se la successione è strettamente descrescente
    - "Non decresente e Non crescente" se la successione non è nè crescente nè decrescente
*/

// N.B: in questo caso creo un array con dimnesione 999, ma se l'utente inserisce più di 1000 numeri l'array non è abastanza capiente

#include <stdio.h>

int main()
{
    int sequenza[999];
    int i = 0, j,corrente, cresc = 1, decresc = 1;

    do {
        printf("Inserisci il valore: ");

        scanf("%d%*c", &sequenza[i]);
        i++;
    } while(sequenza[i-1] != 0);

    corrente = sequenza[0];

    for(j=1; j<i-1; j++) {
        if(sequenza[j] > corrente) {
            corrente = sequenza[j];
            cresc = 1;
        } else {
            cresc = 0;
            j = i;
        }

        if(sequenza[j] < corrente) {
            corrente = sequenza[j];
            decresc = 1;
        } else {
            decresc = 0;
            j = i;
        }
    }

    if(i == 1) //se i==1 vuol dire che ho inserito solo lo 0, quindi non ho inserito nessun valore
        printf("Nessun numero inserito\n");
    else if(i == 2) //se i==2 vuol dire che ho inserito solo un numero e poi lo 0, quindi non ho una sequenza
        printf("Hai inserito un solo valore. Non è una sequenza\n");
    else if(cresc == 1)
        printf("Crescente");
    else if (decresc == 1)
        printf("Decrescente");
    else
        printf("Nè crescente Nè decresente");

    return 0;
}
