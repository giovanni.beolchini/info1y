/*
	Dire cosa stampa questo programma quando l'utente inserisce i valori:
	a) 5	b) 0	 c) -1
*/

#include <stdio.h>
int main ()
{
	int valore = 0;
	printf("\nInserisci il valore: ");
	scanf("%d", &valore);

	if(valore)
		printf("Mi piace l colore rosso.\n");
	else
		printf("Mi piace il colore blu.\n");
	return 0;
}

// a ---> rosso
// b ---> blu
// c ---> rosso

//questo perchè, in generale, l'espressione all'interno dell'if è vera se ha come risultato un valore diverso da 0 (sia positivo che negativo)
