#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <math.h>

typedef int TipoElemento;

typedef struct EL {
  TipoElemento info;
  struct EL * prox;
} ElemLista;

typedef ElemLista * ListaDiElem;

int verificapresenzaric(ListaDiElem lista, TipoElemento info);
void visualizzalistaric(ListaDiElem lista);
int verificapresenzaric(ListaDiElem lista, TipoElemento info);
void distruggilistaric(ListaDiElem lista2);
ListaDiElem insintesta(ListaDiElem lista, TipoElemento elem);
ListaDiElem invertilista(ListaDiElem lista);
ListaDiElem cancellaunoric(ListaDiElem lista, TipoElemento elem);
ListaDiElem insincodaric(ListaDiElem lista, TipoElemento elem);
ListaDiElem elimina_primo(ListaDiElem lista);
int contaelem(ListaDiElem lista);
TipoElemento elem_nodo(ListaDiElem lista, int nodo);

int main(){
	int i;
	ElemLista * lista;
	ElemLista * start2;
	ElemLista * temp;
	lista= (ElemLista *) malloc(sizeof(ElemLista));
	lista->info=1;
	lista->prox=NULL;
	start2=lista;
	for(i=2; i<=6; i++){
		temp= (ElemLista *) malloc(sizeof(ElemLista));
		temp->info=i;
		temp->prox=NULL;
		start2->prox=temp;
		start2=temp;
	}
	visualizzalistaric(lista);
	int d=7;
	if(verificapresenzaric(lista, d)){
		printf("Il %d e' presente\n", d);
	}
	else{
		printf("Il %d non e' presente\n", d);
	}
	lista=insintesta(lista, 0);
	visualizzalistaric(lista);
	lista=cancellaunoric(lista, 6);
	visualizzalistaric(lista);
	lista=insincodaric(lista, 7);
	visualizzalistaric(lista);
	lista=elimina_primo(lista);
	visualizzalistaric(lista);
	printf("Il numero di elementi e': %d\n", contaelem(lista));
	printf("Il numero al nodo 3 e': %d\n", elem_nodo(lista, 3));
	distruggilistaric(lista);
	lista=NULL;
	visualizzalistaric(lista);
	
	return 0;
}



ListaDiElem elimina_primo(ListaDiElem lista){
	ListaDiElem temp=NULL;
	temp=lista->prox;
	free(lista);
	lista=temp;
	return lista;
}

TipoElemento elem_nodo(ListaDiElem lista, int nodo){
	int i;
	for(i=1; i<nodo; i++){
		lista=lista->prox;
	}
	return lista->info;
}

int contaelem(ListaDiElem lista){
	int i=0;
	while(lista!=NULL){
		i++;
		lista=lista->prox;
	}
	return i;
}

ListaDiElem insincodaric(ListaDiElem lista, TipoElemento elem){
	ListaDiElem temp;
	if(lista!=NULL){
		lista->prox=insincodaric(lista->prox, elem);
		return lista;
	}
	else{
		lista= (ListaDiElem) malloc(sizeof(ElemLista));
		lista->info=elem;
		lista->prox=NULL;
		return lista;
	}
}

ListaDiElem cancellaunoric(ListaDiElem lista, TipoElemento elem){
	ListaDiElem temp;
	if(lista->info==elem){
		temp=lista;
		lista=lista->prox;
		free(temp);
		return lista;
	}
	else if(lista->prox!=NULL){
		lista->prox=cancellaunoric(lista->prox, elem);
		return lista;
	}
	else if(lista->prox==NULL){
		return lista;
	}
}

ListaDiElem invertilista(ListaDiElem lista){
	ListaDiElem temp=NULL, lista2;
	lista2=lista;
	for(; lista!=NULL; ){
		temp=insintesta(temp, lista->info);
		lista=lista->prox;
	}
	distruggilistaric(lista2);
	return temp;
}

ListaDiElem insintesta(ListaDiElem lista, TipoElemento elem){
	ListaDiElem temp;
	temp= (ListaDiElem) malloc(sizeof(ElemLista));
	temp->info=elem;
	temp->prox=lista;
	lista=temp;
}


void distruggilistaric(ListaDiElem lista){
	if(!listavuota(lista)){
		distruggilistaric(lista->prox);
		free(lista);
		lista=NULL;
	}
}

int verificapresenzaric(ListaDiElem lista, TipoElemento info){
	if(lista->info==info){
		return 1;
	}
	else{
		if(lista->prox!=NULL){
			return verificapresenzaric(lista->prox, info);
		}
		else{
			return 0;
		}
	}
}

void visualizzalistaric(ListaDiElem lista){
	if(!listavuota(lista)){
		printf("%d", lista->info);
		if(!listavuota(lista->prox)){
			printf("-->");
		}
		lista=lista->prox;
		visualizzalistaric(lista);
	}
	else{
		printf("--|\n");
	}
}

int listavuota(ListaDiElem lista){
	if(lista==NULL){
		return 1;
	}
	else{
		return 0;
	}
}
