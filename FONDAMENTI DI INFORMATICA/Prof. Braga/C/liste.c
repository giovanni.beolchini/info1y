#include <stdio.h>
#include <stdlib.h>

//---------------------- DEFINIZIONE LISTA -----------------------------------------------//
typedef struct nodo
{
    int dato;
    struct nodo* next;

}t_nodo;

//---------------------- DEFINIZIONE NODO LISTA ------------------------------------------//
typedef t_nodo* t_lista;

//---------------------- DICHIARAZIONE PUNTATORE A LISTA E PUNTATORE AUSILIARIO ----------//
t_lista p, p_aux;
int num_nodi = 0;

//---------------------- PROTOTIPI FUNZIONI ----------------------------------------------//
t_lista crea_lista();
void stampa_lista(t_lista ls);

//---------------------- MAIN ------------------------------------------------------------//
int main (void)
{
    t_lista lista_main;

    printf("Quanti nodi vuoi nella lista ?: ");
    scanf("%d", &num_nodi);

    lista_main = crea_lista();

    stampa_lista(lista_main);
    printf("Numero di nodi: %d\n", num_nodi);
    
}

//---------------------- CREA LISTA -----------------------------------------------------//
t_lista crea_lista ()
{   
    if(num_nodi == 0)                                       // se la lista è vuota
    {
        p = NULL;
    }

    p = (t_lista)malloc(sizeof(t_nodo));                    // creazione PRIMO nodo

    printf("Inserire 1^ elemento: ");
    scanf("%d", &p->dato);                                  // PRIMO elemento PRIMO nodo

    p_aux = p;                                              // inizializzazione puntatore ausiliario

    for (int i = 2; i <= num_nodi; i++)
    {
        p_aux->next = (t_lista)malloc(sizeof(t_nodo));      // ALLOCAZIONE nodo N
        p_aux = p_aux->next;                                // CREAZIONE nodo N

        printf("Inserire %d^ elemento: ", i);
        scanf("%d", &p_aux->dato);

    }

    p_aux->next = NULL;                                     // ultimo nodo punta a NULL

    return p;                                               // ritorna PRIMO nodo della lista

}

//---------------------- STAMPA LISTA ---------------------------------------------------//
void stampa_lista (t_lista ls)
{
    printf("LISTA -> ");

    while(ls != NULL)
    {
        printf("%d", ls->dato);
        printf(" -> ");
        ls = ls->next;
    }

    printf("NULL\n");

    return ;
}