/*
Si consideri la seguente struttura dati dinamica per rappresentare una lista di interi:

struct nodo
{
    int dato;
    struct nodo *next;
    struct nodo *nextP;
    struct nodo *nextD;
};

typedef struct nodo *lista;

In  cui  next punta  al  successivo  elemento  della  lista,  nextP punta  all’elemento  successivo  pari  della  lista,  mentre
nextD punta all’elemento successivo dispari della lista.

Implementare una funzione inserisci che consenta di inserire in coda un valore intero all’interno di una lista del tipo qui sopra descritto.

Nota. Si definisca il prototipo della funzione inserisci nel modo ritenuto più opportuno.


*/

struct nodo
{
    int dato;
    struct nodo *next;
    struct nodo *nextP;
    struct nodo *nextD;
};

typedef struct nodo *lista;

void inserisci(lista *l, int n) {
    lista p = malloc(sizeof(struct nodo));
    lista cur = *l;

    p->next = NULL;
    p->nextP = NULL;
    p->nextD = NULL;
    p->dato = n;

    if(*l == NULL)
        *l = p;
    else {
        while(cur->next != NULL) {
            if (cur->nextP == NULL && n%2 == 0)
                cur->nextP = p;
            if (cur->nextD == NULL && n%2 == 1)
                cur->nextD = p;

            cur = cur->next;
        }

        cur->next = p;
        if (n%2 == 0)
            cur->nextP = p;
        else
            cur->nextD = p;
    }
}
